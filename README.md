# BEAutomationTask

This project is a REST API automation project based on Carina framework (https://zebrunner.github.io/carina/). It allows easy manipulation with REST services and convinient verification of api responses.

In terms of tests, currently there are 5 Test classes (one per each endpoint) and each of them has at least 1 positive and few negative tests depending on the context.
Where applicable data driven technique is applied (see example test **signUpNewUser_emptyData** which uses data provider).

## Getting started

Install and configure JDK 11+

Install and configure Apache Maven 3.6.3+

Download the latest version of Intelij Idea Community Edition or any IDE of your choice


## How to run it locally

Clone the project and import it to your IDE
Build the project with Maven
Go to src>test>java and run the desired test class or any specific test marked with @Test annotation

TODO: I will add xml runner which will serve as a Suite runner in order to be able to run all tests easily (either locally either via CI pipeline)
