package com.symphonyis.api;

import com.zebrunner.carina.api.AbstractApiMethodV2;
import com.zebrunner.carina.api.annotation.Endpoint;
import com.zebrunner.carina.api.annotation.SuccessfulHttpStatus;
import com.zebrunner.carina.api.http.HttpMethodType;
import com.zebrunner.carina.api.http.HttpResponseStatusType;
import com.zebrunner.carina.utils.config.Configuration;

@Endpoint(url = "${base_url}/api/posts/${id}/comments/", methodType = HttpMethodType.GET)
@SuccessfulHttpStatus(status = HttpResponseStatusType.OK_200)
public class GetComments extends AbstractApiMethodV2 {
  public GetComments(String id){
    super(null, null, "");
    replaceUrlPlaceholder("base_url", Configuration.getRequired("baseUrl"));
    replaceUrlPlaceholder("id", id);
  }
}
