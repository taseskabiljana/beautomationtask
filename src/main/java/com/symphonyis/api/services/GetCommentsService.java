package com.symphonyis.api.services;

import com.symphonyis.api.GetComments;
import io.restassured.response.Response;

public interface GetCommentsService {
  default Response getCommentsService(String token, String id) {
    GetComments getCommentsService = new GetComments(id);
    getCommentsService.setHeader("Authorization", token);
    return getCommentsService.callAPI();
  }
}
