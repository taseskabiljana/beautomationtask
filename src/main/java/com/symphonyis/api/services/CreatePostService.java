package com.symphonyis.api.services;

import com.symphonyis.api.CreatePost;
import com.symphonyis.api.dtos.CreatePostRequest;
import io.restassured.response.Response;

public interface CreatePostService {
  default Response createPostService(String token, CreatePostRequest createPostRequest) {
    CreatePost createPostEndpoint = new CreatePost();
    createPostEndpoint.setHeader("Authorization", token);
    createPostEndpoint.setRequestBody(createPostRequest);
    return createPostEndpoint.callAPI();
  }
  
  default int getPostId(String token, CreatePostRequest createPostRequest) {
    return createPostService(token, createPostRequest).jsonPath().getInt("id");
  }

}
