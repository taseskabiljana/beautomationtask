package com.symphonyis.api.services;

import com.symphonyis.api.AddComment;
import com.symphonyis.api.dtos.AddCommentRequest;
import io.restassured.response.Response;

public interface AddCommentService {
  default Response addCommentService(String token, AddCommentRequest addCommentRequest) {
    AddComment addComment = new AddComment();
    addComment.setHeader("Authorization", token);
    addComment.setRequestBody(addCommentRequest);
    return addComment.callAPI();
  }
}
