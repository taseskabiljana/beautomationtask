package com.symphonyis.api.services;

import com.symphonyis.api.SignUp;
import com.symphonyis.api.dtos.SignUpRequest;
import io.restassured.response.Response;

public interface SignUpService {
    default Response signUpUser(SignUpRequest signUpRequest) {
    SignUp signUpEndpoint = new SignUp();
    signUpEndpoint.setRequestBody(signUpRequest);
    return signUpEndpoint.callAPI();
  }
}
