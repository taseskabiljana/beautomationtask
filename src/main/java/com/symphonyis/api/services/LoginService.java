package com.symphonyis.api.services;

import com.symphonyis.api.Login;
import com.symphonyis.api.dtos.LoginRequest;
import io.restassured.response.Response;

public interface LoginService {
  default Response loginService(LoginRequest loginRequest) {
    Login loginEndpoint = new Login();
    loginEndpoint.setRequestBody(loginRequest);
    return loginEndpoint.callAPI();
  }

  default String getToken(LoginRequest loginRequest) {
    return "token " + loginService(loginRequest).jsonPath().getString("token");
  }
}
