package com.symphonyis.api.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor

public class LoginRequest {
  @JsonProperty("username")
  private String username;
  @JsonProperty("password")
  private String password;
}
