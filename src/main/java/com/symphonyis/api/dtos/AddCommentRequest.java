package com.symphonyis.api.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor

public class AddCommentRequest {
  @JsonProperty("text")
  private String text;
  @JsonProperty("post")
  private int post;
}
