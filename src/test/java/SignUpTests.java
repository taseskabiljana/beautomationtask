import com.symphonyis.api.services.SignUpService;
import com.symphonyis.api.dtos.SignUpRequest;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public final class SignUpTests extends BaseTest implements SignUpService{

  @Test
  public void signUpNewUser_validData() {
    SignUpRequest signUpRequest = new SignUpRequest(email, password, username, firstName, lastName, dateOfBirth);
    Response signUpResponse = signUpUser(signUpRequest);
    Assert.assertEquals(signUpResponse.statusCode(), HttpStatus.SC_CREATED);
    Assert.assertEquals(signUpResponse.jsonPath().getString("success"), "Thanks for signing up.");
  }

  @Test(dataProvider = "negativeDataCombinations", description = "negative test for signup with empty fields")
  public void signUpNewUser_emptyData(String email, String pass, String user, String fn, String ln, String date) {
    SignUpRequest signUpRequest = new SignUpRequest(email, pass, user, fn, ln, date);
    Response signUpResponse = signUpUser(signUpRequest);
    Assert.assertEquals(signUpResponse.statusCode(), HttpStatus.SC_BAD_REQUEST);
  }

 /* @Test
  public void login_validData() {
    LoginRequest loginRequest = new LoginRequest(username, password);
    Response loginResponse = loginService(loginRequest);
    Assert.assertEquals(loginResponse.statusCode(), HttpStatus.SC_OK);
    token = loginResponse.jsonPath().getString("token");
    Assert.assertEquals(loginResponse.jsonPath().getString("user.username"), username);
  }*/

  @DataProvider(name = "negativeDataCombinations")
  public Object[][] signUpMissingData() {
    return new Object[][]{
        {username, password, "", firstName, lastName, dateOfBirth},
        {"", password, username, firstName, lastName, dateOfBirth},
        {username, "", username, firstName, lastName, dateOfBirth},
        {username, password, username, "", lastName, dateOfBirth},
        {username, password, username, firstName, "", dateOfBirth},
        {username, password, username, firstName, lastName, ""},
    };
  }
}
