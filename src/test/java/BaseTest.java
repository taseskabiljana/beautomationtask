import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.BeforeMethod;

public class BaseTest {
  String username;
  String password = "symphonyis123";
  String email;
  String dateOfBirth="06/07/1999";
  String firstName="FirstName";
  String lastName="LastName";
  String token;
  int postId;
  String comment = "new comment";
  String post = "new post";

  @BeforeMethod
  public void beforeMethod(){
    username = "username"+ RandomStringUtils.random(5, true, true);
    email = "testemail"+ RandomStringUtils.random(5, true, true)+"@test.com";
  }
}
