import com.symphonyis.api.dtos.LoginRequest;
import com.symphonyis.api.dtos.SignUpRequest;
import com.symphonyis.api.services.LoginService;
import com.symphonyis.api.services.SignUpService;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public final class LoginTests extends BaseTest implements LoginService, SignUpService {

  @BeforeMethod
  public void beforeLogin(){
    signUpUser(new SignUpRequest(email, password, username, firstName, lastName, dateOfBirth));
  }

  @Test
  public void login_validData() {
    LoginRequest loginRequest = new LoginRequest(username, password);
    Response loginResponse = loginService(loginRequest);
    Assert.assertEquals(loginResponse.statusCode(), HttpStatus.SC_OK);
    Assert.assertNotNull(loginResponse.jsonPath().getString("token"));
    Assert.assertEquals(loginResponse.jsonPath().getString("user.username"), username);
  }

  @Test(description = "negative test for invalid credentials")
  public void login_wrongCredentials() {
    LoginRequest loginRequest = new LoginRequest(username+1, password+1 );
    Response loginResponse = loginService(loginRequest);
    Assert.assertEquals(loginResponse.statusCode(), HttpStatus.SC_NOT_FOUND);
  }

  @Test(description = "negative test for missing credentials")
  public void login_missingCredentials() {
    LoginRequest loginRequest = new LoginRequest("", password);
    Response loginResponse = loginService(loginRequest);
    Assert.assertEquals(loginResponse.statusCode(), HttpStatus.SC_BAD_REQUEST);
  }
}
