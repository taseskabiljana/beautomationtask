import com.symphonyis.api.dtos.AddCommentRequest;
import com.symphonyis.api.dtos.CreatePostRequest;
import com.symphonyis.api.dtos.LoginRequest;
import com.symphonyis.api.dtos.SignUpRequest;
import com.symphonyis.api.services.AddCommentService;
import com.symphonyis.api.services.CreatePostService;
import com.symphonyis.api.services.LoginService;
import com.symphonyis.api.services.SignUpService;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public final class AddCommentTests extends BaseTest implements SignUpService, LoginService, CreatePostService, AddCommentService {

  int postId;

  @BeforeMethod
  public void beforeAddComment(){
    signUpUser(new SignUpRequest(email, password, username, firstName, lastName, dateOfBirth));
    token = getToken(new LoginRequest(username, password));
    postId = getPostId(token, new CreatePostRequest(post));
  }

  @Test
  public void addComment_validData() {
    Response addComment = addCommentService(token, new AddCommentRequest(comment, postId));
    Assert.assertEquals(addComment.statusCode(), HttpStatus.SC_CREATED);
    Assert.assertEquals(addComment.jsonPath().getString("user.username"), username);
    Assert.assertEquals(addComment.jsonPath().getString("user.username"), username);
    Assert.assertNotNull(addComment.jsonPath().getString("text"), comment);
  }

  @Test(description = "negative test for unauthorized req")
  public void addComment_unauthorized() {
    Response addComment = addCommentService("", new AddCommentRequest(comment, postId));
    Assert.assertEquals(addComment.statusCode(), HttpStatus.SC_UNAUTHORIZED);
  }

  @Test(description = "negative test for non existing post req")
  public void addComment_nonExistingPost() {
    Response addComment = addCommentService(token, new AddCommentRequest(comment, 0));
    Assert.assertEquals(addComment.statusCode(), HttpStatus.SC_BAD_REQUEST);
  }
}
