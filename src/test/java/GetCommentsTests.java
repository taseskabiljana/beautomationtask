import com.symphonyis.api.dtos.AddCommentRequest;
import com.symphonyis.api.dtos.CreatePostRequest;
import com.symphonyis.api.dtos.LoginRequest;
import com.symphonyis.api.dtos.SignUpRequest;
import com.symphonyis.api.services.AddCommentService;
import com.symphonyis.api.services.CreatePostService;
import com.symphonyis.api.services.GetCommentsService;
import com.symphonyis.api.services.LoginService;
import com.symphonyis.api.services.SignUpService;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public final class GetCommentsTests extends BaseTest implements SignUpService, LoginService, CreatePostService, AddCommentService, GetCommentsService {

  @BeforeMethod
  public void beforeGetComments(){
    signUpUser(new SignUpRequest(email, password, username, firstName, lastName, dateOfBirth));
    token = getToken(new LoginRequest(username, password));
    postId = getPostId(token, new CreatePostRequest(post));
    addCommentService(token, new AddCommentRequest(comment, postId));
  }

  @Test
  public void getComments_validData() {
    Response getComments = getCommentsService(token, String.valueOf(postId));
    Assert.assertEquals(getComments.statusCode(), HttpStatus.SC_OK);
    Assert.assertTrue(getComments.jsonPath().getString("results").contains(comment));
  }

  @Test(description = "negative test for unauth req")
  public void getComments_unauthorized() {
    Response getComments = getCommentsService("", String.valueOf(postId));
    Assert.assertEquals(getComments.statusCode(), HttpStatus.SC_UNAUTHORIZED);
  }

  @Test(description = "negative test for non existing postid")
  public void getComments_nonExistingPostId() {
    Response getComments = getCommentsService(token, "xxx");
    Assert.assertEquals(getComments.statusCode(), HttpStatus.SC_NOT_FOUND);
  }

  @Test(description = "negative test for empty postid")
  public void getComments_emptyPostId() {
    Response getComments = getCommentsService(token, "");
    Assert.assertEquals(getComments.statusCode(), HttpStatus.SC_NOT_FOUND);
  }
}
